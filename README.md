# Documents

Documents used for the seminar.

## Overview

- `md/technische_umsetzung.md` \[de\]: describes technical solutions and techniques used for the seminar
- `md/ablauf.md`: \[de\]: describes the sequence of actions for the Seminar
