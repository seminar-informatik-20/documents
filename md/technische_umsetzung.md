# Technisch Umsetzung des Seminars „Webentwicklung mit Python"

## Einführung

- Digitale Präsentation
- Erklären von einfachen Konzepten und Strukturen in Python
- Einführung in Bash, Python, HTML & CSS

## Arbeitsphase

Teilnehmer\*innen bekommen Zugangsdaten zu einem GNU/Linux server. Diese können sie mit ssh zum login auf den Server verwenden. Die bereitgestellten Nutzeraccounts werden keine Rootrechte besitzen und innerhalb eines chroot jails[^1] mit einer Filequota von 1 GB und einer RAM Quota von 500 MB limitiert sein. Nach dem Seminar wird der Code vom Server, bei Bedarf und Zustimmung der Teilnehmer\*innen, extrahiert und an die Teilnehmer\*innen weitergegeben.

Die Schüler können bei der Entwicklung über einen reverseproxy[^2] mit einem Apache Webserver auf die lokal laufenden Flask Instanzen zugreifen. Der Webserver wird hierbei keine personenbezogenen Daten aufzeichnen.

## Fazit

die Präsentation der Projekte erfolgt durch die Teilnehmer\*innen. Es ist herbei nicht erwartet, dass alle Teilnehmer\*innen fertig werden, da das Seminar vor allem die Schüler für das Programmieren mit Python auch nach dem Seminar fördern möchte. Um dies zu unterstützen stellen wir alle genutzten Materialien[^3] und komplette Musterlösungen[^4] für die vorgestellten Projektideen online offen[^5] zur Verfügung.

[^1]: https://en.wikipedia.org/wiki/Chroot
[^2]: https://www.ionos.com/digitalguide/server/know-how/what-is-a-reverse-proxy/
[^3]: https://gitlab.com/seminar-informatik-20/documents & https://gitlab.com/seminar-informatik-20/presentations
[^4]: https://gitlab.com/seminar-informatik-20/simple-projects
[^5]: Alle Dokumente etc. sind unter der GNU GENERAL PUBLIC LICENSE Version 3 @ Seminar Team lizenziert
