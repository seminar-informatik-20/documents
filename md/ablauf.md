# Ablauf des Seminars „Webentwicklung mit Python“

Das Seminar wird nach einer Einführung zur Python und dem arbeiten mit bash/ nano in eine Arbeitsphase übergehen (Siehe Ablaufgrafik). Hierbei wird in der Eingagnsühase die verschiedenen Ebnen des modernen Webs (Server, …) erklärt und danach mit einer Einführung in Python, HTML und CSS weitergemacht.

![Ablaufplan](../assets/ablauf.svg)

Danach werden Projektvorschläge[^1] vorgestellt und die Teilnehmer\*innen können sich in zweier Gruppen (oder einzeln bei genügend Computern) eine Idee aussuchen oder eine eigene bearbeiten.

Hierbei werden die Teilnehmer\*innen bei Problemen oder Fragen natürlich direkt durch das Seminarteam unterstützt.

Zum Schluss werden die verschiedenen Arbeitsfortschritte präsentiert. Es wird anschließend über passende Anschlussveranstaltungen oder Materialen informiert.

Das Ziel des Seminars ist es, zu Arbeit an den Projekten und dem Programmieren in der Zukunft zu motivieren. Hierbei wird auch das Arbeiten mit Linux gefördert um einen direkten Einstieg in einer praktischen Umgebung zu geben.

Um die Arbeit danach weiter zu fördern sind alle verwendeten Dokumente [^2] und komplette Musterlösungen [^1] für die vorgestellten Projektideen öffentlich und frei [^3] verfügbar.

[^1]: https://gitlab.com/seminar-informatik-20/simple-projects
[^2]: <https://gitlab.com/seminar-informatik-20/documents> & <https://gitlab.com/seminar-informatik-20/presentations>
[^3]:
    Alle Dokumente etc. sind unter der
    GNU GENERAL PUBLIC LICENSE Version 3 @ Seminar Team lizenziert
