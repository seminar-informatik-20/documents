# Datenschutz Konzept

Mit NUTZER\*INNEN sind alle Teilnehmer\*innen des Seminars, welche die bereitgestellten Geräte benutzen und/ oder sich auf dem bereitgestellten Server mit ihren Nutzerdaten einloggen. DATEN sind alle aufgezeichneten Verbindungen, Ein- und Ausgaben in der BASH und die „personenbezogene“ Liste der Verbindungen von Pseudonymen und Namen der NUTZER\*INNEN. Begriffsbestimmungen sind am Artikel 4 der DSGVO orientiert und Schlüsselbegriffe sind weiterhin mit „Begriff“ hervorgehoben.

Dieses Dokument ist eine _Übersicht_ um eine Überblick zu geben und kein _rechtlich bindendes_ Dokument.

## Datensammlung

Die einzigen „personenbezogene“ Daten, welche gesammelt, werden sind Liste der Verbindungen von Pseudonymen und Namen der NUTZER\*INNEN. Alle anderen Daten sind pseudonomisiert.

Bei einer Verbindung über SSH wird die IP-Adresse und das Pseudonym gespeichert. Die IP-Adresse ist hierbei Anonym, da alle Nutzer über das interne Netzwerk der Schule sich mit dem Server verbinden und dessen IP nicht zurückverfolgbar ist. Die über diese Verbindung eingegebenen Zeichen sowie die daraus resultierenden Aufgaben werden direkt mit script[^1] aufgezeichnet.

## Datenweitergabe & Datenverarbeitung

Es findet keine „Datenweitergabe“ oder „Datenverarbeitung“ des gesammelten DATEN statt. Alle DATEN werden gemäß der Datenaufbewahrung nach dem Seminar behandelt.

## Pseudonymisierung

Eine „Pseudonymisierung“ wird verwendet. Hierbei kriegen die NUTZER\*INNEN ein zufälliges Pseudonym. Die Verbindung dieses Pseudonyms mit dem Namen des/ der NUTZER\*INNEN ist nur dem/ der Lehrer\*in bekannt. Diese\*r Lehrer\*in hat keinen Zugriff auf die DATEN. Den Zugriff auf die DATEN in Rohform hat nur der Verwalter des Servers, welcher keinen Zugriff auf die Liste hat.

Bei einem Sicherheitszwischenfall, innerhalb des Seminars, der auf ein Pseudonym, durch die gesammelten DATEN, zurückgeführt werden kann wird das Pseudonym an den Lehrer weitergegeben, sodass dieser bei Haltbarkeitsfragen zusammen mit dem Verwalter und den haftbaren Personen der\*s NUTZER\*INNEN eine Lösung finden kann.

## Datenaufbewahrung

All DATEN werden für maximal 3 Tage nach der Beendigung des Seminars aufbewahrt. Der beim Seminar produzierte Programmcode wird auf Wunsch extrahiert und an den/ die NUTZER\*INNEN weitergegeben. Um die sichere Datenvernichtung zu gewährleisteten und mögliche Artefakte zu vernichten wird der Server und die bereitgestellten Laptops komplett neu, innerhalb von maximal drei Tagen, aufgesetzt.

Die physische Liste der Verbindungen von Pseudonymen und Namen der NUTZER\*INNEN wird nicht kopiert und nach maximal drei Tagen vernichtet.

[^1]: https://manpages.debian.org/buster/bsdutils/script.1.en.html
