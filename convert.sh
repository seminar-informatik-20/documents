#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# Uses private mdpdf tool
# GPLv3.0 @ Cobalt <https://cobalt.rocks>

cd md

for file in *.md; do
    echo "Converting $file to pdf"
    mdpdf "$file" "../pdf/${file:0:-3}.pdf" -dpn
done

cd ..
